<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>User Management Login Form</title>

<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet"
	href="resources/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="resources/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="resources/assets/css/form-elements.css">
<link rel="stylesheet" href="resources/assets/css/style.css">

<!-- Favicon and touch icons -->
<link rel="shortcut icon" href="resources/assets/ico/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="resources/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="resources/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="resources/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="resources/assets/ico/apple-touch-icon-57-precomposed.png">
	
<!-- Javascript -->
	<script src="resources/assets/js/jquery-1.11.1.min.js"></script>
	<script src="resources/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/assets/js/jquery.backstretch.min.js"></script>
	<script src="resources/assets/js/scripts.js"></script>

</head>

<body>
		<div class="container">

			<form class="form-horizontal">
				<fieldset>

					<!-- Form Name -->
					<legend>
						<b style="color: white;">REGISTRATION</b>
					</legend>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="fn">First name</label>
						<div class="col-md-4">
							<input id="fn" name="fn" type="text" placeholder="First name"
								class="form-control input-md" required="">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="ln">Last name</label>
						<div class="col-md-4">
							<input id="ln" name="ln" type="text" placeholder="Last name"
								class="form-control input-md" required="">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="email">Email</label>
						<div class="col-md-4">
							<input id="email" name="email" type="text" placeholder="Email"
								class="form-control input-md" required="">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="city">Street</label>
						<div class="col-md-4">
							<input id="city" name="city" type="text" placeholder="Street"
								class="form-control input-md" required="">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="zip">City</label>
						<div class="col-md-4">
							<input id="zip" name="zip" type="text" placeholder="City"
								class="form-control input-md" required="">

						</div>
					</div>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="zip">State</label>
						<div class="col-md-4">
							<select id="selectbasic" name="selectbasic"
								class="form-control input-md" style="height: 50px;">
								<option>Please select state</option>
								<option>Option two</option>
							</select>
						</div>
					</div>
					<!-- Select Basic -->
					<!-- <div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Select Basic</label>
  <div class="col-md-4">
    <select id="selectbasic" name="selectbasic" class="form-control input-md">
      <option>Option one</option>
      <option>Option two</option>
    </select>
  </div>
</div> -->
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="ctry">Country</label>
						<div class="col-md-4">
							<select id="selectbasic" name="selectbasic"
								class="form-control input-md" style="height: 50px;">
								<option>Please select country</option>
								<option>Option two</option>
							</select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="add1">Address</label>
						<div class="col-md-4">
							<input id="add1" name="add1" type="text" placeholder="Address"
								class="form-control input-md" required="">

						</div>
					</div>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="zip">Zip Code</label>
						<div class="col-md-4">
							<input id="zip" name="zip" type="text" placeholder="Zip Code"
								class="form-control input-md" required="">

						</div>
					</div>


					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="phone">Phone
							Number</label>
						<div class="col-md-4">
							<input id="phone" name="phone" type="text"
								placeholder="Phone Number" class="form-control input-md"
								required="">

						</div>
					</div>

					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="selectbasic">Date
							of Birth</label>
						<div class="col-md-1">
							<select id="selectbasic" name="selectbasic"
								class="form-control input-md"
								style="width: 137%; margin: 6px; height: 50px;">
								<option>Date</option>
								<option>Option two</option>
							</select>
						</div>
						<div class="col-md-1">
							<select id="selectbasic" name="selectbasic"
								class="form-control input-md"
								style="width: 137%; margin: 6px; height: 50px;">
								<option>Month</option>
								<option>Option two</option>
							</select>
						</div>
						<div class="col-md-1">
							<select id="selectbasic" name="selectbasic"
								class="form-control input-md"
								style="width: 137%; margin: 6px; height: 50px;">
								<option>Year</option>
								<option>Option two</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="Training">Gender</label>
						<div class="col-md-4">
							<label class="radio-inline" for="Training-0"> <input
								type="radio" name="Training" id="Training-0" value="yes"
								checked="checked"> Male
							</label> <label class="radio-inline" for="Training-1"> <input
								type="radio" name="Training" id="Training-1" value="no">
								Female
							</label>
						</div>
					</div>


					<!-- Button -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="submit"></label>
						<div class="col-md-4">
							<button id="submit" name="submit" class="btn btn-primary">SUBMIT</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>






	</div>
	<!-- Model for forgot password end-->

	


</body>

</html>