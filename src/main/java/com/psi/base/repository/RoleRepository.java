package com.psi.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.psi.base.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
	
	Role findByRoleName(String roleName);
	
	

}
