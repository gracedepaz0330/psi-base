package com.psi.base.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterController {

	@RequestMapping("/user-registration")
	public ModelAndView userRegistration(HttpSession session) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("user-management/user-registration");
		return mav;
	}

}
