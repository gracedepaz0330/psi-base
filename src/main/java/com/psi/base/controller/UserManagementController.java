package com.psi.base.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.psi.base.bean.UserDetailBean;
import com.psi.base.exceptions.ExistingUserException;
import com.psi.base.service.UserManagementService;

@Controller
@RequestMapping("user-management")
public class UserManagementController {
	
	@Autowired
	UserManagementService userManagementService;
	
	
	public ModelAndView registerUser(UserDetailBean userDetail) {
		ModelAndView mav = new ModelAndView();
		try {
			if(userManagementService.equals(userDetail.getUserName())) {
				throw new ExistingUserException();
			}
		} catch(ExistingUserException eu) {
			
		} catch(Exception e) {
			
		}
		return mav;
	}

}
