package com.psi.base.bean;

import java.io.Serializable;
import java.util.Date;

public class UserDetailBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String userName;
	
	private String password;
	
	private String createdBy;
	
	private Date createdOn;
	
	private String updatedBy;
	
	private Date updatedOn;
	
	private Integer failedAttempts;
	
	private Date lastLoggedIn;
	
	private String reasonOfDeactivation;
	
	private Date dateOfDeactivation;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getFailedAttempts() {
		return failedAttempts;
	}

	public void setFailedAttempts(Integer failedAttempts) {
		this.failedAttempts = failedAttempts;
	}

	public Date getLastLoggedIn() {
		return lastLoggedIn;
	}

	public void setLastLoggedIn(Date lastLoggedIn) {
		this.lastLoggedIn = lastLoggedIn;
	}

	public String getReasonOfDeactivation() {
		return reasonOfDeactivation;
	}

	public void setReasonOfDeactivation(String reasonOfDeactivation) {
		this.reasonOfDeactivation = reasonOfDeactivation;
	}

	public Date getDateOfDeactivation() {
		return dateOfDeactivation;
	}

	public void setDateOfDeactivation(Date dateOfDeactivation) {
		this.dateOfDeactivation = dateOfDeactivation;
	}
}
