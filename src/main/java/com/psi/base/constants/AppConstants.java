package com.psi.base.constants;

public class AppConstants {

	public static final String USER_ROLE_ADMIN =  "ADMIN";
	public static final String USER_ROLE_NORMAL = "NORMAL";
}
