package com.psi.base.service;

import com.psi.base.model.User;

public interface SecurityService {
	
	User changePassword(User user);

}
