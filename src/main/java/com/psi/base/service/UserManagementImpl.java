package com.psi.base.service;


import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.psi.base.constants.AppConstants;
import com.psi.base.model.Role;
import com.psi.base.model.User;
import com.psi.base.repository.RoleRepository;
import com.psi.base.repository.UserRepository;

@Service("userManagementService")
public class UserManagementImpl implements UserManagementService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

	public User registerUser(User user) {
		Role userRole = roleRepository.findByRoleName(AppConstants.USER_ROLE_NORMAL);
		
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		
		userRepository.save(user);
		return null;
	}

	public boolean deleteUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isUserExist(String userName) {
		User user = userRepository.findUserByUserName(userName);
		if(user == null) {
			return false;
		}
		return true;
	}

}
