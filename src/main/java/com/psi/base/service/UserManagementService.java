package com.psi.base.service;

import com.psi.base.model.User;

public interface UserManagementService {
	
	User registerUser(User user);
	
	boolean deleteUser(User user);
	
	User updateUser(User user);
	
	boolean isUserExist(String userName);

}
